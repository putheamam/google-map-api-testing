package com.developer.android.googlemapsimulation;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class CarTravelSimulation extends AppCompatActivity implements OnMapReadyCallback{
    private MapView mapView;
    private GoogleMap googleMap;
    private Location userCurrentLocation;
    private Location fackLocation;
    private Marker userMarker;
    private FusedLocationProviderClient locationProviderClient;
    private boolean isStop = false;
    private LocationCallback locationCallback;

    private TextView tv_currentAddress;
    private Button bt_start, bt_getAddress, bt_stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_travel_simulation);

        // init
        locationProviderClient = LocationServices.getFusedLocationProviderClient(CarTravelSimulation.this);
        userCurrentLocation = new Location(String.valueOf(locationProviderClient));
        fackLocation = new Location(String.valueOf(locationProviderClient));

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                LatLng latLng = new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());
                moveCamera(locationResult.getLastLocation());
            }
        };

        // init view
        bt_start = findViewById(R.id.button_start);
        bt_getAddress = findViewById(R.id.button_get_add);
        bt_stop = findViewById(R.id.button_stop);
        tv_currentAddress = findViewById(R.id.textview_current_location);


        mapView = findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        getCurrentLocationFromIntent(new CallbackFun() {
            @Override
            public void onCompletedListener(Boolean isSuccess) {
                if(isSuccess){
                    // Load map object
                    mapView.getMapAsync(CarTravelSimulation.this);
                }
            }
        });

        // action
        bt_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLocationUpdated();
            }
        });

        bt_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationProviderClient.removeLocationUpdates(locationCallback);
            }
        });

        bt_getAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng currentLocation = userMarker.getPosition();
                if(currentLocation != null){
                    reverseGeocode(currentLocation.latitude, currentLocation.longitude);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mapView.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (userCurrentLocation != null) {
            moveCamera(userCurrentLocation);
        }else {
            Toast.makeText(CarTravelSimulation.this, "Current location = null", Toast.LENGTH_LONG).show();
        }
    }

    private void moveCamera(Location location){
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        googleMap.animateCamera(cameraUpdate);
        setMarker(latLng);
    }

    private void setMarker(LatLng latLng){
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLatitude());
        if (userMarker == null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.draggable(true);
            markerOptions.title("You are here.");
            markerOptions.position(latLng);
            BitmapDescriptor carIcon = BitmapDescriptorFactory.fromResource(R.drawable.car1);
            markerOptions.icon(carIcon);
            userMarker = googleMap.addMarker(markerOptions);
        }
        userMarker.setPosition(latLng);
    }

    private void getCurrentLocationFromIntent(CallbackFun callbackFun){
        //get passed location
        if(getIntent().hasExtra("lat") && getIntent().hasExtra("long")){
            Location location;
            if(getIntent().getDoubleExtra("lat", 0) != 0){
                userCurrentLocation.setLatitude(getIntent().getDoubleExtra("lat", 0));
            }else finish();

            if(getIntent().getDoubleExtra("long", 0) != 0){
                userCurrentLocation.setLongitude(getIntent().getDoubleExtra("long", 0));
            }else finish();


        }

        if(userCurrentLocation != null){
            if(userCurrentLocation.getLatitude() != 0 && userCurrentLocation.getLatitude() != 0){
                callbackFun.onCompletedListener(true);
            }else {
                finish();
            }
        }else {
            finish();
        }
    }

    @SuppressLint("MissingPermission")
    public void onLocationUpdated(){
        LocationRequest request = new LocationRequest();
        request.setInterval(1000);
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationProviderClient.requestLocationUpdates(request, locationCallback, null);
    }

    private void reverseGeocode(double lat, double lng){
        Geocoder geocoder = new Geocoder(this);
        try {
            Log.d("Car", "Start reverse geocoding...");
            tv_currentAddress.setText("Lat: " + String.valueOf(lat) + ", Long: " + String.valueOf(lng) );
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Log.d("Car", "Finish reverse geocoding...");
            if(!addresses.isEmpty()) {
                Address address = addresses.get(0);
                tv_currentAddress.setText(address.getAddressLine(0));
                Log.d("Car", "Address: " + address);
            }else {
                Log.e("Car", "Address empty");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        DoAsynTask doAsynTask = new DoAsynTask();
        doAsynTask.execute(1.444, 234.56);
    }

    private class DoAsynTask extends AsyncTask<Double, Void, Double> {

        @Override
        protected Double doInBackground(Double... doubles) { // only this function that run in background and can't access to ui component
            // call onProgressUpdate to fire onProgressUpdate function
            onProgressUpdate();
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) { // onProgressUpdate is used to access ui component and update them during task executing
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Double aDouble) { // onPostExecute is fired after task executed
            super.onPostExecute(aDouble);
        }
    }
}
