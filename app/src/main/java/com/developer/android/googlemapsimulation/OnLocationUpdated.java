package com.developer.android.googlemapsimulation;

public interface OnLocationUpdated {
    void onLocationUpdated();
}
